public with sharing class AccountAuraController {
    private static AccountService service = AccountService.getInstance();

    @AuraEnabled
    public static Map<String, String> getFieldLabels(List<String> fields) {
        return service.getFieldLabels(fields);
    }

    @AuraEnabled
    public static List<Account> getAccounts(List<String> fields) {
        List<Account> result = null;
        try {
            result = service.getAccounts(fields);
        } catch (DmlException e) {
            throw new AuraHandledException(e.getMessage());
        }
        return result;
    }
}
/**
 * Created by alexanderbelenov on 11.06.2022.
 */

public with sharing class AccountService {
    private AccountService() {}

    public Map<String, String> getFieldLabels(List<String> fields) {
        Schema.SObjectType accountSchema = Account.getsObjectType();
        Map<String, Schema.SObjectField> fieldMap = accountSchema.getDescribe().fields.getMap();
        Map<String, String> result = new Map<String, String>();
        for (String field : fields) {
            final String label = fieldMap.get(field).getDescribe().getLabel();
            result.put(field, label);
        }
        return result;
    }

    public List<Account> getAccounts(List<String> fields) {
        final String query = 'SELECT ' + String.join(fields, ', ') + ' FROM Account';
        List<Account> result = Database.query(query);
        return result;
    }

    private static AccountService instance = null;

    public static AccountService getInstance() {
        if (instance == null) {
            instance = new AccountService();
        }
        return instance;
    }
}
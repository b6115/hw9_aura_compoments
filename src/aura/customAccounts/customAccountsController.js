({
	init : function(component, event, helper) {
        const listOfFieldsText = component.get("v.listOfFields");
        const requiredField = component.get("v.requiredField");
        const allFieldNames = (listOfFieldsText==null? `${requiredField}` : `${listOfFieldsText},${requiredField}`);
        helper.setTableHeader(component, allFieldNames);
        helper.setTableData(component, allFieldNames);
	}
})
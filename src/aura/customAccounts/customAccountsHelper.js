({
	setTableData: function(component, fieldsText) {
        const fieldsList = fieldsText.split(',');
        const action = component.get("c.getAccounts");
        action.setParams({
            "fields": fieldsList
        });
        action.setCallback(this, function(response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                const data = response.getReturnValue();
            	component.set('v.data', data);
            }
            else if (state === "ERROR") {
                const errors = response.getError();
                if (errors) {
                    for (let error of errors) {
                        console.log(error);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    setTableHeader: function(component, fieldsText) {
        const fieldsList = fieldsText.split(',');
        const action = component.get("c.getFieldLabels");
        action.setParams({
            "fields": fieldsList
        });
        action.setCallback(this, function(response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                const fieldLabelMap = new Map(Object.entries(response.getReturnValue()));
                const columns = [];
                fieldLabelMap.forEach(function(labelText, fieldText) {
                    const columnObj = {
                        label: labelText,
                        fieldName: fieldText,
                        type: "text"
                	}
                    columns.push(columnObj);
                });
            	component.set('v.columns', columns);
            }
        });
        $A.enqueueAction(action);
    }
})